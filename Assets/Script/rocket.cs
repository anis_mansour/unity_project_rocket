﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class rocket : MonoBehaviour
{
    [SerializeField]float rcsThrust = 100f; // rcsThrust  'reaction control system 'value to roate speed and serializeflied is to be able to change the value from unity and able to add it as a component to other obj
    [SerializeField] float mainThrust = 100f;
    [SerializeField] AudioClip mainEngine;
    [SerializeField] AudioClip DeadSound;
    [SerializeField] float levelLoadDelay = 2f;
    [SerializeField] AudioClip WinSound;
    [SerializeField] ParticleSystem EngineParticles;
    [SerializeField] ParticleSystem DeadParticles;
    [SerializeField] ParticleSystem WinParticles;
    Rigidbody rigidBody;
    bool CollisionToggle = true; 
    enum State { Alive, Dying, Transcending}
    State state = State.Alive;
    AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();



    }

    // Update is called once per frame
    void Update()
    {
        if (state == State.Alive) {  //free fall if dead 
        Thrust();
        GoDown();
        Rotate();
            TestMode();
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if(state != State.Alive || CollisionToggle == false) { return; } //if dead stop the game otherwise it keeps printing dead 


        switch (collision.gameObject.tag)
        {
            case "friendly":
                print("friendly"); //do nothing
                break;
            case "Finish":
                state = State.Transcending;
                print(state);
                audioSource.PlayOneShot(WinSound);
                WinParticles.Play();
                Invoke("NextLevel", levelLoadDelay);  //invoke need the method name to be a string , seconds to start method
                //NextLevel(); call the funtion in invoke as a string 
                break;
            default:
                
                state = State.Dying;
                audioSource.PlayOneShot(DeadSound);
                DeadParticles.Play();
                Invoke("FirstLevel", levelLoadDelay);           
                break;
        }
    }

    private void FirstLevel()
    {
        SceneManager.LoadScene(0);
    }

    private void NextLevel()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        print(currentSceneIndex);
        int nextSceneIndex = currentSceneIndex + 1;
        if(nextSceneIndex == SceneManager.sceneCountInBuildSettings)
        { nextSceneIndex = 0; }
        SceneManager.LoadScene(nextSceneIndex);
       
    }



    private void Thrust()
    {
        if (Input.GetKey(KeyCode.Space)|| Input.GetKey(KeyCode.UpArrow))
        {
            rigidBody.AddRelativeForce(Vector3.up * mainThrust *Time.deltaTime);
            if (!audioSource.isPlaying)
            {
                audioSource.PlayOneShot(mainEngine);
            }
            EngineParticles.Play();


        }
        else { audioSource.Stop();
            EngineParticles.Stop();
        }
    }

    private void GoDown()
    {
        if (Input.GetKey(KeyCode.DownArrow))

        {
            rigidBody.AddRelativeForce(Vector3.down * mainThrust * Time.deltaTime);
        }
    }
    private void Rotate()
    {
        rigidBody.freezeRotation = true; // take control of rotation

        float rotationThisFrame = rcsThrust * Time.deltaTime;



        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            transform.Rotate(Vector3.forward * rotationThisFrame);
        }
        else if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {

            transform.Rotate(-Vector3.forward * rotationThisFrame);
        }
        rigidBody.freezeRotation = false; //resume regular control of rotation
    }


    //can change it to build mode so it doesnt work when deployed

  
    private void TestMode()
    {
        if (Input.GetKeyDown(KeyCode.N)){
            NextLevel();
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            CollisionToggle = !CollisionToggle;
    }
}
    

}
