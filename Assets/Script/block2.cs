﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class block2 : MonoBehaviour
{



    [SerializeField] Vector3 movementVector = new Vector3(10f, 10f, 10f);
    [SerializeField] float period = 2f;
    [Range(0, 1)] [SerializeField] float movementFactor;   //0 for not moved 1 for full
    Vector3 startingPos;
    // Start is called before the first frame update
    void Start()
    {
        startingPos = transform.position;

    }

    // Update is called once per frame
    void Update()
    {
        if(period <= Mathf.Epsilon) { return; } // to avoid / by zero   getting error
        float cycles = Time.time / period; // keep growing from 0

        const float tau = Mathf.PI * 1;   // speed 
        float rawSinWave = Mathf.Sin(cycles * tau);

        movementFactor = rawSinWave / 2f + 0.5f;

        Vector3 offset = movementVector * movementFactor;
        transform.position = startingPos + offset;
    }
}
